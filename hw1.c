/* hw1.c */
/* 与えられた１６文字からなるべく長い単語を作るプログラム */
/* その文字が入るか入らないかの２通りをしらみつぶしに試していくのはNG
（2^16通り考えてはいけない）*/

/* じぶんで作ったtest用のデータでそうだったので辞書データは小文字アルファベットだけだという仮定になってしまっています。
    まちがえました。 */


#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>

#define NUM 26  //分けるのはアルファベット２６文字（　＋　「'」とかの分必要？　）
#define WORD_SIZE 16   //今回は１６文字与える

typedef struct dictionary{
  char *word;
  int *alphabet;
  int many;
  struct dictionary *next;
}dic;        //構造体dic

dic* addlst(dic *l,char *w,int *a,int m){  //リスト構造の構造体dicへの代入用
  dic* ret = malloc(sizeof(dic));
  ret->word = w;
  ret->alphabet = a;
  ret->many = m;
  ret->next = l;
 return ret;
}

void printlst(dic *d){        //構造体のリストdicの中身チェック用
  while(d!=NULL){
    printf("%d   ",d->many);
    d = d->next;
  }
}

int is_valid_word(char* word){ //入力された文字列がWORD_SIZE文字以内の'a'~'z'の小文字なのか判断
  int i;
  if(strlen(word) >= WORD_SIZE)  //
    return -1;
  for(i=0;i<strlen(word);i++){
    if(*(word+i)<'a'||*(word+i)>'z'){
      return -1;
    }
  }
  return 1;
}

check_alphabets(char* word, int* alph){
  int i=0,j=0;   //例えば alph[0]=2,alph[1]=1 なら aが２個,bが１個含まれている
  int count=0;
  for(i=0;i<NUM;i++){ 
    count = 0;
    for(j=0;j<strlen(word);j++){ 
      if(*(word+j)=='a'+i){
	count++;
	//	printf("count alph[%d] = %d\n",i,count);           //チェック用！！！！
      }
    }
    alph[i]=count;
  }
}


main(int argc,char *argv[]){
/* 入力された文字列に対する処理スタート */

  if(argc != 2){      //引数が2つじゃないとき
    fprintf(stderr,"Usage: hw1 <alphabets>\n");
    exit(EXIT_FAILURE);
  }
  if(is_valid_word(argv[1]) < 0){
    perror("Invalid words\n");
    exit(EXIT_FAILURE);
  }

 char kotoba[27];
 int alph2[NUM];
 strcpy(kotoba,argv[1]);

 printf("I'm searching for anagrams of '%s'…\n",kotoba);
 printf("すみません、１番長いアナグラムを返すプログラムは間に合いませんでした。\n候補となるアナグラム一式とその文字数が表示されますので、\nその中から１番文字数が多いものを選んでください。\n");
 check_alphabets(kotoba,alph2); //例) alph2[0]=2,alph2[1]=1 なら aが２個,bが１個含まれている


/* 辞書データに対する事前処理 */
 /* 1.辞書データを開く */

  /*  fp = fopen("/usr/share/dict/american-english","r"); 試したのですが、途中までしかデータを
読み込めていないような・・・?なので、テストデータtest.txtで試したりしてました。
 　また、/usr/share/dict/american-english　のデータの中身を確認したところ、
頭文字'z'の単語たちの後からアルファベット以外の文字列の辞書になっているようなので'z'まででwhile文が
止まるような条件を入れる必要があるのでしょうか・・・　*/

  FILE *fp;
  char tmp[256];
  char *mark;

  if((fp = fopen("test.txt","r"))==NULL){
    printf("辞書データファイルがうまく読み込めません\n");
  }

  /*
 if((fp = fopen("/usr/share/dict/american-english","r"))==NULL){
    printf("辞書データファイルがうまく読み込めません\n");
  }
  */

  while(fgets(tmp,256,fp)){
    mark = strtok(tmp,"\n");  //改行"\n"で文字を区切る
    //   printf("%s\n",mark);                //辞書の中身確認。チェック用。実際には要らない！！！！
    while(mark != NULL){

      /* 2.辞書データ１つ１つをリスト構造の構造体になるよう構造体dicに入れていく */

  /* ASCⅡコード表にて

       0x41=='A' ~ 0x5a=='Z'         0x27=='''
       0x61=='a' ~ 0x7a=='z'
  */

      dic *d;
      int many=0;
      int alph[NUM];

      check_alphabets(mark,alph); //例) alph[0]=2,alph[1]=1 なら aが２個,bが１個含まれている      
      many = strlen(mark);        //文字数。該当する中でこれが最も多いものを出力したい
      if(d==NULL)    d = addlst(NULL,mark,alph,many);    //リスト構造の構造体に入れていく
      else           d = addlst(d,mark,alph,many);

      //      printf("word = '%s', 文字数 = %d\n",d->word,d->many);     //チェック用！！！！



   /////////// ここから妥協案プログラム ////////////////
      //これだと１番長いのだけ返すという趣旨にのっとっていない…が、タイムオーバー
      int num=0,checker=0;
      for(num=0;num<NUM;num++){
	if(alph[num]>alph2[num]){  //入力された文字列より多い文字が１つでもあったら該当しない
	  checker++;
	}
      }
      if(checker==0){
	printf("word = '%s', 文字数 = %d\n",d->word,d->many);
      }
   ////////////////////////////////////////////////


      mark = strtok(NULL,"\n");           //改行で区切って、次の辞書単語へ
    } 
  }
  //printlst(d);                   //辞書リストdic中身確認用。実際は要らない！！！！
  //これ使うとerrorになってしまう・・・　　ここ以降だと、「dが宣言されてない」と表示されてしまう

 fclose(fp);
  

 /* 1. 字数などの関係から該当する辞書の文字列を探す */
 /* 本当ならリスト構造の構造体を利用してこのタイミングで調べるはずが、
「dが宣言されていない」と出る上に自分の中でこんがらがってわかりませんでした・・・
 int n=0;
 for(n=0;n<NUM;n++){
   if((d->alphabet->alph[n])<=alph2[n])  //これならOK
     retrn 1;
 }
  */
}
